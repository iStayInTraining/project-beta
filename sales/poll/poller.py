import django
import os
import sys
import time
import requests
import json

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

INVENTORY_API_URL = "http://project-beta-inventory-api-1:8000/api/automobiles/"


def poll(repeat=True):
    while True:
        print("Service poller polling for data")
        try:
            response = requests.get(INVENTORY_API_URL)
            content = json.loads(response.content)
            for auto in content["autos"]:
                AutomobileVO.objects.update_or_create(vin=auto["vin"])
        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(1)


if __name__ == "__main__":
    poll()
