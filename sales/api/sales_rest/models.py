from django.db import models
from django.urls import reverse


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.employee_id}"


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.phone_number}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"AutomobileVO: {self.vin} - {'Sold' if self.sold else 'Available'}"


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
    )
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"Sale: {self.automobile.vin} - {self.salesperson.first_name} {self.salesperson.last_name} - {self.customer.first_name} {self.customer.last_name} - ${self.price}"

    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"id": self.id})
