from django.shortcuts import render
from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from django.http import JsonResponse, HttpResponseNotFound
from datetime import datetime, date
from django.views.decorators.http import require_http_methods
import json
from .models import Salesperson, Customer, Sale, AutomobileVO
from decimal import Decimal


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime, date)):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["id", "first_name", "last_name", "employee_id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "first_name", "last_name", "address", "phone_number"]

    def default(self, o):
        if isinstance(o, Sale):
            return {
                "id": o.id,
                "automobile": o.automobile,
                "salesperson": o.salesperson,
                "price": o.price,
            }
        return super().default(o)


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price"]

    def get_api_url(self, o):
        return {"api_url": o.get_api_url}

    def default(self, o):
        if isinstance(o, AutomobileVO):
            return {"vin": o.vin, "sold": o.sold}

        elif isinstance(o, Salesperson):
            return {
                "id": o.id,
                "first_name": o.first_name,
                "last_name": o.last_name,
                "employee_id": o.employee_id,
            }

        elif isinstance(o, Customer):
            return {
                "id": o.id,
                "first_name": o.first_name,
                "last_name": o.last_name,
                "address": o.address,
                "phone_number": o.phone_number,
            }

        elif isinstance(o, Decimal):
            return str(o)

        return super().default(o)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

    def __str__(self):
        return f"AutomobileVO: {self.vin} - {'Sold' if self.sold else 'Available'}"


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request, id=None):
    if request.method == "GET":
        if id is not None:
            salespeople = Salesperson.objects.filter(id=id)
        else:
            salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople}, encoder=SalespersonEncoder, safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request, id):
    try:
        salesperson = Salesperson.objects.get(id=id)
    except Salesperson.DoesNotExist:
        return HttpResponseNotFound("Salesperson not found.")

    if request.method == "GET":
        return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request, id=None):
    if request.method == "GET":
        if id is not None:
            customers = Customer.objects.filter(id=id)
        else:
            customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers}, encoder=CustomerEncoder, safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, encoder=CustomerEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
    except Customer.DoesNotExist:
        return HttpResponseNotFound("Customer not found.")

    if request.method == "GET":
        return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        salesperson_id = request.GET.get("salesperson")

        if salesperson_id is not None:
            try:
                salesperson = Salesperson.objects.get(id=salesperson_id)
                sales = Sale.objects.filter(salesperson=salesperson)
            except Salesperson.DoesNotExist:
                return JsonResponse({"message": "Invalid salesperson ID"}, status=400)
        else:
            sales = Sale.objects.all()

        return JsonResponse({"sales": sales}, encoder=SaleEncoder, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            automobile_vo_vin = content["automobile"]
            automobile_vo = AutomobileVO.objects.get(vin=automobile_vo_vin)
            content["automobile"] = automobile_vo
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Invalid automobile VIN"}, status=400)

        salesperson_id = content.get("salesperson")
        customer_id = content.get("customer")

        if salesperson_id is not None:
            try:
                salesperson = Salesperson.objects.get(id=salesperson_id)
                content["salesperson"] = salesperson
            except Salesperson.DoesNotExist:
                return JsonResponse({"message": "Invalid salesperson ID"}, status=400)

        if customer_id is not None:
            try:
                customer = Customer.objects.get(id=customer_id)
                content["customer"] = customer
            except Customer.DoesNotExist:
                return JsonResponse({"message": "Invalid customer ID"}, status=400)

        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    try:
        sale = Sale.objects.get(id=id)
    except Sale.DoesNotExist:
        return HttpResponseNotFound("Sale not found.")

    if request.method == "GET":
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
