# CarCar

Team:

* Person 1 - Sean Cunningham = Service
* Person 2 - David Enriquez - sales microservice

## Design

## Service microservice

Models: AutomobileVO (for the poller), Technician, Appointment, Status.
encoders: each of the above models has an associated encode, some have both detail and list encoders.
views:
    api_list_statuses(GET, POST): allows me to create the required status value objects to aggregate with the appointment mode.
    api_list_technicians(GET, POST): allows for creating and retreiving a list of techs
    api_shoe_technicians(GET, PUT, DELETE): allows for retreiving data, updating, or deleting a single instance of a tech.
    api_list_appointments(GET, POST): allows for creating and retreiving a list of appts
    api_show_appointments(GET, PUT, DELETE): allows for retreiving data, updating, or deleting a single instance of appts
    api_finish_appointment and api_cancel_appointment(PUT): allows for changing the status VO on the appointment instance to finished' or 'canceled' respectively.
Poller: Poller works with an inventory api and utilized the retreived data to instantiate aoutomobileVOs.

React Components: list form components were created for the inventory models as well as the appointment and technician models. the forms all were menat to create additional instances of their respective model. the list functions are built one of two ways. the first is that they map through the data and populate that directly into the html tables. the other includes the additional creation of components that represent a table row. when iterating through the data these are creeated and passed data as props. A few buttons were created to add to list items. while not really necesary, this was to practice creating and using additional child elements.
Nested routes are included in the App.js function.
Additional nav links were created for each of the main list/form components.


## Sales microservice

Explain your models and integration with the inventory
microservice, here.

The models created for the sales microservice are defined as model classes: "Salesperson", "Customer", "AutomobileVO", and "Sale".

The Salesperson model contains first_name, last_name, employee_id fields.

The Customer model containing first_name, last_name, address, and phone_number fields.

The Sale model containing automobile, salesperson, customer and price fields. All fields except for price should be foreign key fields.

The AutomobileVO model containing vin and sold fields.

The integration with the inventory microservice is done in the poll function of the poller.py file in the sales microservice. We are getting updated data from the inventory microservice.

Created React components for the frontend of the sales microservice. These components display the backend data when it is fetched. The views we defined in the backend are handling our API calls from the frontend for the sales microservice data.
