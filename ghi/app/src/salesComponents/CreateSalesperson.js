import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateSalesperson()
{
    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => setFirstName(event.target.value);
    const handleLastNameChange = (event) => setLastName(event.target.value);
    const handleEmployeeIdChange = (event) => setEmployeeId(event.target.value);

    const handleSubmit = async (event) =>
    {
       event.preventDefault();
       const data = {};
       data.first_name = firstName;
       data.last_name = lastName;
       data.employee_id = employeeId;

        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig =
        {
            method: "post",
            body: JSON.stringify(data),
            headers:
            {
                "Content-Type": "application/json"
            },
        };

        try
        {
            const response = await fetch(salespersonUrl, fetchConfig);
            if (!response.ok)
            {
                console.error('Error adding a salesperson');
            }
            else
            {
                const newSalesperson = await response.json();
                setFirstName('');
                setLastName('');
                setEmployeeId('');
                navigate('/sales/salespeople');
            }
        }
        catch (error)
        {
            console.error(error);
        }
    };
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange}
                value={firstName}
                placeholder="first name"
                required type="text"
                id="firstName"
                className="form-control"
                name="firstName" />
                <label htmlFor="firstName">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleLastNameChange}
                value={lastName}
                placeholder="last name"
                required type="text"
                id="lastName"
                className="form-control"
                name="lastName" />
                <label htmlFor="lastName">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleEmployeeIdChange}
                value={employeeId}
                placeholder="employee ID"
                required type="text"
                id="employeeId"
                className="form-control"
                name="employeeId" />
                <label htmlFor="employeeId">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default CreateSalesperson;
