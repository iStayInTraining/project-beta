import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function CustomerList() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    async function fetchCustomers() {
      try {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (!response.ok) {
          console.error("Error loading customers data");
        } else {
          const data = await response.json();
          setCustomers(data.customers);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchCustomers();
  }, []);

  return (
    <div>
      <h2>Customers List</h2>
      <Link to="/sales/customer" className="btn btn-primary">Add a Customer</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer => (
            <tr key={customer.id}>
              <td>{customer.first_name} {customer.last_name}</td>
              <td>{customer.phone_number}</td>
              <td>{customer.address}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default CustomerList;
