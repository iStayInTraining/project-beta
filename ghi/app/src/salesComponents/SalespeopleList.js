import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function SalespeopleList() {
   const [salespeople, setSalespeople] = useState([]);
   useEffect(() => {
      async function fetchSalesperson() {
         try {
            const response = await fetch("http://localhost:8090/api/salespeople/")
            if (!response.ok) {
               console.error("Error loading salespeople data");
            } else {
               const data = await response.json();
               setSalespeople(data.salespeople);
            }
         } catch (error) {
            console.error(error);
         }
      }
      fetchSalesperson();
   }, []);

   return (
      <div>
      <h2>Salespeople List</h2>
      <Link to="/sales/salesperson" className="btn btn-primary">Add a Salesperson</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(salesperson => (
            <tr key={salesperson.id}>
               <td>{salesperson.first_name}</td>
               <td>{salesperson.last_name}</td>
               <td>{salesperson.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
   );
}

export default SalespeopleList;
