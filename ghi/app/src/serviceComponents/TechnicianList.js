import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function TechnicianList() {

  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    async function fetchTechnicians() {
      try {
        const response = await fetch("http://localhost:8080/api/technicians/");
        if (!response.ok) {
          console.error("Error loading manufacturers data");
        } else {
          const data = await response.json();
          setTechnicians(data.technicians);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchTechnicians();
  }, []);

  return (
    <div>
      <h2>Technicians List</h2>
      <Link to="/service/technician/new" className="btn btn-primary">Create New Technician</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(tech => (
            <tr key={tech.id}>
              <td>{`${tech.first_name} ${tech.last_name}`}</td>
              <td>{tech.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
