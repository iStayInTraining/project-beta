function FinishAppointmentButton({id, finish}) {
    return <button className="btn btn-success" onClick={()=>{finish(id)}}>Finish</button>
  }
  export default FinishAppointmentButton
