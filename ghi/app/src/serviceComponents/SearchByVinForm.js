import { useState } from "react";


function SearchByVinForm({appointments, filter}){

    const [appts, setAppts] = useState(appointments);
    const [vinChoice, setVinChoice] = useState('')


    function handleSubmit(event){
        event.preventDefault();
        filter(vinChoice)
    }


    const handleVinChange = (event) => {
        setVinChoice(event.target.value);
    }


    return (
        <div className="row">
            <div className=" col-12">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New automobile</h1>
                    <form onSubmit={handleSubmit} id="vin-search-form">
                    <div>
                        <select onChange={handleVinChange} value={vinChoice} required id="vinChoice" className="form-select" name="vinChoice" multiple={false}>
                            <option value="">Vin</option>
                                {[...(new Set(appointments.map(app=>app.vin)))].map(vin=>{
                                return (
                                <option value={vin} key={vin}>{vin}</option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Fetch Records</button>
                </form>
            </div>
            </div>
        </div>
    );

}

export default SearchByVinForm;
