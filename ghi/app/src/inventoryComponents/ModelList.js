import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ModelList() {

  const [models, setModels] = useState([]);

  useEffect(() => {
    async function fetchModels() {
      try {
        const response = await fetch("http://localhost:8100/api/models/");
        if (!response.ok) {
          console.error("Error loading manufacturers data");
        } else {
          const data = await response.json();
          setModels(data.models);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchModels();
  }, []);

  return (
    <div>
      <h2>Vehicle Models List</h2>
      <Link to="/new/model" className="btn btn-primary">Create New Vehicle Model</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map(model => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td><img src={model.picture_url} alt={`${model.manufacturer.name} ${model.name}`} /></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ModelList;
